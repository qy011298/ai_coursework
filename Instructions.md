## Instructions:

Both files were developed in Visual Studio Code, using Python 3.8.7 64-bit.

To run _ga_continuous-distrib.py_ or _ga-combinatorial-distrib.py_, from this folder:

```bash
<PATH/TO/PYTHON/INTERPRETER>.exe ./ga_continuous-distrib.py
<PATH/TO/PYTHON/INTERPRETER>.exe ./ga-combinatorial-distrib.py
```

**Note:** Running the _City.py_ will do nothing.

To access the different problems that each GA implementation is setup to solve, the `FITNESS_CHOICE` variable can be changed within the file. See the code, or the report, or the section further down for details on which problems are being solved. I would have done this via parsing command line arguments, but Visual Studio Code can't easily do this. Also, I'm not being marked on my ability to parse arguments in Python. 

For _ga_continuous-distrib.py_ , Different Selection / Crossover / Mutation methods can be selected by changing the following variables.

| Value | SELECTION_CHOICE         | CROSSOVER_CHOICE       | MUTATION_CHOICE      |
|-------|--------------------------|------------------------|----------------------|
|1      | Roulette Wheel Selection | Middle Point Crossover | Single Gene Mutation |
|2      | Steady State Selection   | Multi Point Crossover  | Multi Gene Mutation  |

This GA implementation also contains a user variable test-bench, that was used to generate all the tests results in the report. 

(Copied and Pasted from Report)

```bash
RUN_TESTS: Boolean: Parameter to signify that tests will be run.

CHANGED_VAR: String: A string that refers to the variable that will be changed. Can take the
following values: “POPULATION”, “MUTATION”, “CROSSOVER”, “PARENT_NO”, “NONE”.

Note: “NONE” will not change any variable per run. Used to produce the control test.

START_VAL: Real: This defines the beginning value of the CHANGED_VAR variable.

DIFFERENCE: Real: The difference in the CHANGED_VAR variable, per test.

NO_OF_TESTS: Integer: Number of Genetic Algorithm runs that are done.

Example:

If CHANGED_VAR = “POPULATION”, START_VAL = 10, DIFFERENCE = 5 and NO_OF_TESTS = 3.
3 Genetic Algorithm Runs would occur, with the Population value being 10,15 and 20 respectively.
```

For _ga-combinatorial-distrib.py_, the `Elitism` variable can be set to `True` or `False`, which will apply Elitism to the GA Run. 

In both .py files, the `Debug` variable can be set to `True`, if additional debug output is required. This can often produce a lot of output if the population size is particularly large, or many generations are being run.

### FITNESS_CHOICE Value:

| Value | _ga_continuous-distrib.py_ | _ga-combinatorial-distrib.py_ |
|-------|----------------------------|-------------------------------|
| 1     | Sum Squares                | Sum 1s (Minimisation)         |
| 2     | Weighted Input             | Sum 1s (Maximisation)         |
| 3     | Levy                       | Reach Target Sentence         |
| 4     | Trid                       | Reach Target Number (Hex)     |
| 5     | Griewank                   | Knapsack Problem              |
| 6     | Zakharov                   | Travelling Salesman           |  

 
